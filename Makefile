SHELL:=/bin/bash
ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent
BASE_PATH=${PWD}
PYTHON_EXEC=python
DOCKER_COMPOSE_FILE=$(shell echo -f docker-compose.yml -f docker-compose.override.yml)
VENV_PATH=~/.venv/vertc-ops

ifeq ($(OS),Windows_NT)
    OSFLAG=WIN32
    ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
        OSFLAG=AMD64
    else
        ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
            OSFLAG=AMD64
        endif
        ifeq ($(PROCESSOR_ARCHITECTURE),x86)
            OSFLAG=IA32
        endif
    endif
else
    UNAME_S := $(shell uname -s | tr A-Z a-z)
    ifeq ($(UNAME_S),linux)
        OSFLAG=LINUX
    endif
    ifeq ($(UNAME_S),darwin)
        OSFLAG=OSX
    endif
    UNAME_P := $(shell uname -p)
    ifeq ($(UNAME_P),x86_64)
        OSFLAG=AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        OSFLAG=IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        OSFLAG=ARM
    endif
endif

include .env
export $(shell sed 's/=.*//' .env)


black: 
	docker-compose exec web black */*.py

isort:
	docker-compose exec web isort */*.py

flake:
	docker-compose exec web flake8 */*.py


fix_permissions:
	sudo chown -R ${USER}:${USER} .
    
format: isort fix_permissions flake

build:
	docker-compose up -d --remove-orphans --build

up: 
	docker-compose up -d --remove-orphans

restart: 
	docker-compose restart "${ARGS}"

stop: 
	docker-compose stop "${ARGS}"

stopall: 
	docker-compose stop

down: 
	docker-compose down --remove-orphans

logs: 
	docker-compose logs --tail 200 -f "${ARGS}"

log: 
	docker-compose logs --tail 200 -f web

makemigrations:
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate

migrate:
	docker-compose exec web python manage.py migrate '${ARGS}'


createsuperuser:
	docker-compose exec web python manage.py createsuperuser --noinput --email admin@admin.com

seed:
	docker-compose exec web python seed.py

init: down build makemigrations createsuperuser fix_permissions seed
